﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="ARGO_Web.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
    <title>ARGO WEB Tool </title>



</head>
<body runat="server">
    <div class="jumbotron" runat="server">

        <h1 class="display-4 text-center">Welcome to ARGO Web Tool!</h1>
        <p class="lead text-center">it's good to have you here please follow the steps</p>
        <hr class="my-4" />
        <p>Please, click in the following buttom to Set the variables</p>
    </div>

    <div class="container" runat="server">
        <p>
            <button class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button"
                aria-expanded="false" aria-controls="multiCollapseExample1">
                Set Values</button>
        </p>
        <div class="row" runat="server">
            <div class="col" runat="server">
                <div class="collapse multi-collapse" id="multiCollapseExample1" runat="server">
                    <div class="card card-body" runat="server">
                        <form runat="server">
                            <div class="form-group row" runat="server">
                                <label for="inputClientName" class="col-sm-2 col-form-label" runat="server">Client Name</label>
                                <div class="col-auto" runat="server">
                                    <input type="text" class="form-control" id="inputClientName" value="ex:123" runat="server"/>
                                </div>
                            </div>
                            <div class="form-group row" runat="server">
                                <label for="inputClientID" class="col-sm-2 col-form-label" runat="server">Client ID</label>
                                <div class="col-auto" runat="server">
                                    <input type="text" class="form-control" id="inputClientID" placeholder="ex:234" runat="server"/>
                                </div>
                            </div>
                            <div class="form-group row" runat="server">
                                <%--<button id="btnExecute" type="submit" class="btn btn-info" runat="server" onclick="btnExecute_Click">Execute SSIS</button>--%>
                                <asp:Button ID="btnExecute" CssClass="btn btn-info" runat="server" Text="Execute SSIS" OnClick="btnExecute_Click" />
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
