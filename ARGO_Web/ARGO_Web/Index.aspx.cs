﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SqlServer.Dts.Runtime;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;



namespace ARGO_Web
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnExecute_Click(object sender, EventArgs e)
        {
            Application app = new Application();
            Package package = null;
            try
            {
                //string fileName = Server.MapPath(System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName.ToString()));
                //FileUpload1.PostedFile.SaveAs(fileName);

                //Load DTSX
                package = app.LoadPackage(@"C:\Users\jsaenzadm\source\repos\automated-extract-handler\ARGOSSIS\Package.dtsx", null);

                //Global Package Variable
                Variables vars = package.Variables;
                vars["vClientName"].Value = Request.Form["inputClientName"].ToString();
                vars["vClientID"].Value = Request.Form["inputClientID"].ToString();

                //Specify Excel Connection From DTSX Connection Manager
               // package.Connections["SourceConnectionExcel"].ConnectionString = "provider=Microsoft.Jet.OLEDB.4.0;data source=" + fileName + ";Extended Properties=Excel 8.0; ";

                //Execute DTSX.
                Microsoft.SqlServer.Dts.Runtime.DTSExecResult results = package.Execute();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                package.Dispose();
                package = null;
            }
        }
    }
}